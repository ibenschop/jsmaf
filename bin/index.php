<!doctype html>
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<style type="text/css">
			body {
				padding:0px;
				margin:0px;
				margin-top:160px;
                background-color: black;
			}
			
			#expanded {
				width: 100%;
				height: 558px;
				background-color:#000;
				margin:0px auto;
			}
		</style>
		<script src="js/vendor/jquery-1.7.1.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="root">
        </div>
        <script src="js/vendor/jsmaf.min.js" type="text/javascript"></script>
        <script src="js/package.min.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>

	</body>
</html>
