mason.pkg("com.jsmaf.demo.modules").module("ViewModuleBoxDetails", {

    Events:{

        GOTO:"ViewModuleBoxDetails.goto"
    },

    Class:ViewModuleBoxDetails = function(__view){

             this.$super("constructor",__view);


        ViewModuleBoxDetails.prototype.bindEvents = function(){

            var self = this;

            this.view.box2Button.click = function(e){

                self.dispatcher.dispatchEvent(self.pkm.event("ViewModuleBoxDetails").GOTO,this,{target:"intro"});
            }
        }
    }
}).extend("ViewModule");