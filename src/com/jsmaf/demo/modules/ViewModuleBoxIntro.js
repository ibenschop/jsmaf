mason.pkg("com.jsmaf.demo.modules").module("ViewModuleBoxIntro",{

    Events:{

        GOTO:"ViewModuleBoxIntro.GoTo"
    },


    Class:ViewModuleBoxIntro = function(__view){

             this.$super("constructor",__view);


        ViewModuleBoxIntro.prototype.bindEvents = function(){

            var self = this;

            this.view.box1Button.click = function(e){

                self.dispatcher.dispatchEvent(self.pkm.event("ViewModuleBoxIntro").GOTO,this,{target:"details"});
            }
        }
    }





} ).extend("ViewModule");