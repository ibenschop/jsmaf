mason.pkg("com.jsmaf.demo.modules").module("ViewModuleBoxContact",{

    Events:{

        GOTO:"ViewModuleBoxContact.GoTo"
    },


    Class:ViewModuleBoxContact = function(__view){

             this.$super("constructor",__view);


        ViewModuleBoxContact.prototype.bindEvents = function(){

            var self = this;


            this.view.box3Button.click = function(e){

                self.dispatcher.dispatchEvent(self.pkm.event("ViewModuleBoxContact").GOTO,this,{target:"details"});
            }
        }
    }





}).extend("ViewModule") ;