mason.pkg("com.jsmaf.demo.modules").module("HelloWorldModule",{

    Class:HelloWorldModule = function(){

        this.name = "helloWorldModule"

       HelloWorldModule.prototype.startup = function(){

           console.log("hello world");

           var self = this;

           $(window).on("click",function(){

               self.dispatcher.dispatchEvent("HelloWorld",this,null);
           })
       }

       HelloWorldModule.prototype.response = function(){

           console.log("my name is "+this.name)

       }

    }

}).extend("BasicModule");

