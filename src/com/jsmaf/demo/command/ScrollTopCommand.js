mason.pkg("com.jsmaf.demo.command").module("ScrollTopCommand",{

    Class:ScrollTopCommand = function (__props) {

        this.y = __props.y || 0;
        this.time = __props.time || .8;

        ScrollTopCommand.prototype.execute = function (e) {

            this.$super("execute");
            if(this.y != NaN)
            TweenLite.to(this.target, this.time, {scrollTop:this.y, ease:Quad.easeInOut,onComplete:this.onComplete,onCompleteScope:this});

        }
    }
}).extend("Command");