mason.pkg("com.jsmaf.demo.command").module("HelloWorldCommand",{

    Class:HelloWorldCommand = function (__props) {

        this.props = __props;

        HelloWorldCommand.prototype.execute = function (e) {


            this.$super("execute");

            console.log("Hello World!")
            this.onComplete();
        }
    }
}).extend("Command");