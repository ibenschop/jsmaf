mason.pkg("com.jsmaf.utils").util("EventManager",{

    Class:EventManager = function(__dispatcher){

        EventManager.prototype.add = function(__event,__callback,__target){

            this.dispatcher.addEventListener(__event,__callback,__target) ;
        }

        EventManager.prototype.remove = function(__event,__callback,__target){

           this.dispatcher.removeEventListener(__event,__callback,__target) ;
        }

    }
});

