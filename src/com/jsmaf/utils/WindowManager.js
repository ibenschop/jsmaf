mason.pkg("com.jsmaf.utils").module("WindowManager", {

    Events:{

        Resize:"WindowManager.Resize"
    },

    Class:WindowManager = function () {

        var namespace = com.jsmaf.utils.WindowManager
        var timer = 0;
        var self = this;

        this.dispatcher = null;


        function init(){

            window.onresize = function () {

                if (timer > 0)return;
                timer = setTimeout(function () {

                    timer = 0;

                    if(self.dispatcher!=null){

                        self.dispatcher.dispatchEvent(namespace.Events.Resize,self,null);
                    }

                }, 180);
            }
        }


        Object.defineProperties(this, {
            "topOffset": {
                "get": function() {   return window.pageYOffset || document.documentElement.scrollTop;}
                },
            "scrollTopOffset":{
                "get": function() {

                    var offset = this.topOffset;

                    if(offset!=0 && offset<window.innerHeight){

                        return (window.innerHeight -  offset)+offset;
                    }else{

                        return NaN;
                    }
                }

            }

        })



        WindowManager.prototype.topOffset = function(){

            return window.pageYOffset || document.documentElement.scrollTop;
        }

        WindowManager.prototype.scrollTopOffset = function(){


        }


        WindowManager.prototype.getWidth = function () {

            return {

                valueOf:function () {

                    return window.innerWidth;
                }
            }

        }


        WindowManager.prototype.getHeight = function () {

            return {

                valueOf:function () {

                    return window.innerHeight;
                }
            }

        }

        init();


    }
});