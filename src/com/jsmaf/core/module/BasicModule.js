mason.pkg("com.jsmaf.core.module").module("BasicModule", {

    Class:BasicModule = function () {

        this.context = null;
        this.dispatcher = null;
        this.pkm = null;
        this.tag =  "BasicModule"


        BasicModule.prototype.register = function(__context)
        {
            if(__context!=undefined){

                this.context = __context;
                this.dispatcher = this.context.dispatcher;
                this.pkm = this.context.pkm;

            }else{

                return;
            }
        }

        BasicModule.prototype.startup = function () {

            console.log("startup")
        }

        return this;
    }
});


