mason.pkg("com.jsmaf.core.module").module("ViewModule", {

     Class:ViewModule = function(__view){


         this.view = null;


         ViewModule.prototype.setView = function(__view){

            this.view = __view;
             return this;
         }

         ViewModule.prototype.startup = function(){

             //this.view.box1Somebox.position = "absolute";
             //this.view.box1Somebox.x = "20%";
             this.bindEvents();
             return this;
         }


         ViewModule.prototype.show = function(){

             return this;
         }

         ViewModule.prototype.hide = function(){

             return this;
         }

         ViewModule.prototype.bindEvents = function(){

             return this;
         }

         ViewModule.prototype.unBindEvents = function(){

             return this;
         }

         return this;
     }
} );

