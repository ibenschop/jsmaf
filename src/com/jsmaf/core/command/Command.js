mason.pkg("com.jsmaf.core.command").module("Command",{



        Events:{

            Complete:"Complete"

        },

        Class:Command = function () {


            this.target = null;
            this.chainID = "";

            function validate() {

                if (this.target != null || this.target != undefined)return false;
            }

            Command.prototype.execute = function () {

                if (!validate())return;

            };

            Command.prototype.destroy = function () {

                this.chainID = null;
                this.target = null
            }

            Command.prototype.undo = function () {

                if (!validate())return;
            }

            Command.prototype.onComplete = function () {


                if (this.dispatchEvent != undefined) {

                    this.dispatchEvent(mason.event("Command").complete, this, null);
                }
            }

        }
});