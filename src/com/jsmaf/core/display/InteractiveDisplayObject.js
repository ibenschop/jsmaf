mason.pkg("com.jsmaf.core.display").module("InterActiveDisplayObject", {

    InteractiveDisplayObject:InteractiveDisplayObject = function(__what){

        this.$super("constructor",__what);

        this.cursor = "pointer";

        Object.defineProperties(this, {
            "click": {
                "get": function() { return  this.element.onclick},
                "set": function(__value) {

                    this.element.onclick = null;
                    this.element.onclick = __value;
                }
            },
            "doubleclick": {
                "get": function() { return  this.element.ondblclick},
                "set": function(__value) {

                    this.element.ondblclick = null;
                    this.element.ondblclick = __value;
                }
            },
            "onmousedown": {
                "get": function() { return  this.element.onmousedown},
                "set": function(__value) {

                    this.element.onmousedown = null;
                    this.element.onmousedown = __value;
                }
            },
            "onmousemove": {
                "get": function() { return  this.element.onmousemove},
                "set": function(__value) {

                    this.element.onmousemove = null;
                    this.element.onmousemove = __value;
                }
            },
            "onmouseover": {
                "get": function() { return  this.element.onmouseover},
                "set": function(__value) {

                    this.element.onmouseover = null;
                    this.element.onmouseover = __value;
                }
            },
            "onmouseout": {
                "get": function() { return  this.element.onmouseout},
                "set": function(__value) {

                    this.element.onmouseout = null;
                    this.element.onmouseout = __value;
                }
            },
            "onmouseup": {
                "get": function() { return  this.element.onmouseup},
                "set": function(__value) {

                    this.element.onmouseup = null;
                    this.element.onmouseup = __value;
                }
            }
        });
    }
});