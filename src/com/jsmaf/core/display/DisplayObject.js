mason.pkg("com.jsmaf.core.display").module("DisplayObject",{

    Class:DisplayObject = function(__what) {


        this.element = document.getElementById(__what);

        Object.defineProperties(this, {
            "x": {
                "get": function() { return  this.element.style.left;},
                "set": function(__value) { this.element.style.left = __value; }
            },
            "alpha": {
                "get":  function(){
                    return this.element.style.opacity;
                },
                "set": function(__value){

                    this.element.style.opacity = __value;
                }
            },
            "position": {
                "get":  function(){

                    return this.element.style.position
                },
                "set":  function(__value){

                    this.element.style.position = __value;
                }
            },
            "height": {
                "get":  function(){

                    return this.element.style.height
                },
                "set":  function(__value){

                    this.element.style.height = __value;
                }
            },
            "width": {
                "get":  function(){

                    return this.element.style.width
                },
                "set":  function(__value){

                    this.element.style.width = __value;
                }
            },
            "display": {
                "get":  function(){

                    return this.element.style.display
                },
                "set":  function(__value){

                    this.element.style.display = __value;
                }
            },
            "cursor": {
                "get":  function(){

                    return this.element.style.cursor
                },
                "set":  function(__value){

                    this.element.style.cursor = __value;
                }
            }
        });
    }
} );