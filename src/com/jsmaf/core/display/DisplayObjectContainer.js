mason.pkg("com.jsmaf.core.display").module("DisplayObjectContainer", {

    Class:DisplayObjectContainer = function(__what){

        this.$super("constructor",__what);


        this.children = {}

        DisplayObjectContainer.prototype.addChild = function(__child){


            var child = {};
            return;
            if(__child.id== ""){

                var id =  Math.random().toString(36).substring(10);
                __child.id = id;
                child.id = id;

            }
            child.el =this.element.appendChild(__child);

            return child;
        }

        DisplayObjectContainer.prototype.removeChild = function(__child){

            var element = document.getElementById(__child);
            this.element.removeChild(element);
        }

        DisplayObjectContainer.prototype.addChildAt = function(__child,__at){

            this.element.insertBefore(__child, this.element.children[__at]);
        }

        DisplayObjectContainer.prototype.removeChildAt = function(__at){

            var element = this.element.children[__at];
            this.element.removeChild(element);
        }
    }
});