mason.pkg("com.jsmaf.core.context").module("BasicContext",{

    Events:{

        Startup:"Startup"
    },

    Class:BasicContext = function(){


        this.dispatcher = null;
        this.viewport = null;
        this.modules = [];
        this.pkm = null;

        BasicContext.prototype.addModule = function (__module,__name,__startup) {


            this.modules.push({module:__module,name:__name,startup:__startup || true});
            __module.register(this);
        }

        BasicContext.prototype.getModule = function(__name){

            var i = 0;
            var l = this.modules.length

            for(i; i<l; ++i){

                var item = this.modules[i];
                if(item.name === __name){

                    return item.module;
                    break;
                }
            }
        }

        BasicContext.prototype.startup = function () {

            var i = 0;
            var l = this.modules.length;

            for(i; i<l; ++i){

                var item = this.modules[i];
                if(item.startup == true)item.module.startup();
            }
            if(this.dispatcher!=null) this.dispatcher.dispatchEvent(mason.event("BasicContext").Startup, this, null);

        }
    }
});