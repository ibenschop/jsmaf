/**
 * User: bootstrap
 * Date: 12/23/13
 * Time: 7:08 PM
 */

(function(window){

    var Mason = function(){

        var classes = {};
        var packages = {};
        var shift = [].shift;
        var slice = [].slice;
        var events = {};
        var utils = {};
        var currentClass = null;

        function extend(__super){

            var $super =  classes[__super];

            var superman = new $super();
            currentClass.prototype = superman;
            currentClass.prototype.$super = function(__fn){

                var args = slice.call(arguments, 1);

                if(__fn == "constructor") {
                    $super.call(this,args);
                }else{

                    currentClass.prototype[__fn].call(this,args);
                }
            }
        }

        function getPath(__path,__namespace){

            if(__path == null){

                if(packages[__namespace] == undefined){

                    packages[__namespace]= {};
                    return  packages[__namespace];
                }else{

                    return packages[__namespace];
                }
            }else{

                if(__path[__namespace] == undefined){

                    __path[__namespace]= {};
                    return  __path[__namespace];
                }else{

                    return __path[__namespace];
                }
            }
        }

        function pkg (__package){

            var split = __package.split(".");
            var i = 0;
            var l = split.length;
            var artifact = null;

            var path = null;

            for(i;i<l;++i){

                if(artifact == null){

                    path = artifact = getPath(path,split[i]);
                }else{

                    path =  getPath(path,split[i]);
                }

            }
            return this;
        }


        //TODO make package depended
        function addPackage(__package){

            for(var i in __package){

                if(i != "Events")this.classes[i] = __package[i];

            }
        }

        function module(__module,__class){

          classes[__module] = __class.Class;
          events[__module] = __class.Events;
            currentClass  = __class.Class;

            return this;
        }

        //TODO make package depended
        function create(__class){

            var result = classes[__class];
            var clsClass =classes[__class]

            if(arguments.length>1){

                var args = Array.prototype.slice.call(arguments, 1);

                function builder() {
                    return clsClass.apply(this, args);
                }

                builder.prototype = clsClass.prototype;
                return new builder();
            } else{

                return new clsClass();
            }

            var args = Array.prototype.slice.call(arguments, 1);

            return new result(args);

        }

        function getClass(__class){

            try{

                var result = classes[__class][__class];
            } catch(e){

                if(e.name == "TypeError")throw new Error("TypeError","Class of type "+__class+" is unknown");

            }

            return result;
        }

        function util(__util,__object){

            if(__object !=undefined){

                return classes[__util];
            }else{

                util[__util] = __object;
            }
        }

        function event(__class){

            return events[__class];
        }

        function command (__command,__target,__props){

            var cls = classes[__command][__command];
            var instance = new cls(__props);
            instance.target = __target;
            return instance;
        }

        function includer(__prefix,__scripts,__callback){

            var src;
            var prefix = __prefix
            var scripts = __scripts;
            var pendingScripts = [];
            var firstScript = document.scripts[0];

            function stateChange() {

                var pendingScript;

                while (pendingScripts[0] && pendingScripts[0].readyState == 'loaded') {
                    pendingScript = pendingScripts.shift();
                    pendingScript.onreadystatechange = null;
                    firstScript.parentNode.insertBefore(pendingScript, firstScript);
                }
            }

            function load(){

                window.onload = function(){

                   __callback();
                }

                while (src = scripts.shift()) {
                    if ('async' in firstScript) { // modern browsers
                        script = document.createElement('script');
                        script.async = false;
                        script.src = prefix+"/"+src.replace(/[.]/g,"/")+".js";
                        document.head.appendChild(script);
                    }
                    else if (firstScript.readyState) { // IE<10
                        script = document.createElement('script');
                        pendingScripts.push(script);
                        script.onreadystatechange = stateChange;
                        script.src =  prefix+"/"+src.replace(/[.]/g,"/")+".js";
                    }
                    else {
                        document.write('<script src="' + src + '" defer></'+'script>');
                    }
                }
            }

            return{

                obj:this,
                load:load
            }

        }

        return{
            obj:this,
            command:command,
            create:create,
            addPackage:addPackage,
            util:util,
            event:event,
            module:module,
            pkg:pkg,
            extend:extend,
            include:includer
        }
    }

    window.mason = new Mason();
})(window);
