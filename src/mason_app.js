/**
 * User: bootstrap
 * Date: 12/18/13
 * Time: 8:20 PM
 */

(function(){

    mason.include("",[
        "com.jsmaf.core.command.Command",
        "com.jsmaf.core.context.BasicContext",
        "com.jsmaf.core.dispatcher.EventDispatcher",
        "com.jsmaf.core.module.BasicModule",
        "com.jsmaf.core.module.ViewModule",
        "com.jsmaf.demo.modules.HelloWorldModule"
    ], function(){

        var context = mason.create("BasicContext");
        var dispatcher = mason.create("EventDispatcher");
        context.dispatcher = dispatcher;

        var helloWorldModule = mason.create("HelloWorldModule");
        context.addModule(helloWorldModule);

        context.startup();
    }
   ).load();

})();
