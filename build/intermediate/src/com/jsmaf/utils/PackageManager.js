var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.PackageManager = {

    PackageManager:PackageManager = function(){

        this.classes = {};
        this.packages = {};
        var n = [].shift;

        PackageManager.prototype.add = function(__package){

            for(var i in __package){

                if(i != "Events")this.classes[i] = __package[i];

            }
        }

        PackageManager.prototype.addClass = function(__name,__class){

            this.classes[__name] = __class;
        }

        PackageManager.prototype.create = function (__class){


            var result = this.classes[__class][__class];
            var clsClass = this.classes[__class][__class];

            if(arguments.length>1){

                var args = Array.prototype.slice.call(arguments, 1);

                function builder() {
                    return clsClass.apply(this, args);
                }
                builder.prototype = clsClass.prototype;
                return new builder();
            } else{


                return new clsClass();
            }
            var args = Array.prototype.slice.call(arguments, 1);


            return new result(args);

        }

        PackageManager.prototype.getClass = function(__class){

            try{

                var result = this.classes[__class][__class];
            } catch(e){

                if(e.name == "TypeError")throw new Error("TypeError","Class of type "+__class+" is unknown");

            }

            return result;
        }

        PackageManager.prototype.util = function(__util){

            var result = this.classes[__util];
            return result;
        }

        PackageManager.prototype.event= function(__class){

            var result =this.classes[__class];
            return result.Events;
        }

        PackageManager.prototype.command = function(__command,__target,__props){

            var cls = this.classes[__command][__command];
            var instance = new cls(__props);
            instance.target = __target;
            return instance;
        }
    }
}


