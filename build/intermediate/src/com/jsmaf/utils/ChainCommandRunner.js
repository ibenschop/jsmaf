mason.pkg("com.jsmaf.utils").util("ChainCommandRunner",{

    Events:{

        Complete:"ChainCommandRunner.Complete"
    },

    Class:function(__chain,__callback,__destroy){

        var count = 0;
        var current = null;
        var next = null;
        var chain = __chain || [];
        var chainLength = chain.length;
        var doDestroy = __destroy || false;

        this.method.destroy = function(){

             count = 0;
             current = null;
             chain = [];
        }


        this.method.run = function(){


            if(count>=chainLength){

                if(this.dispatchEvent!=undefined){

                    if (this.dispatchEvent != undefined) {

                        this.dispatchEvent(mason.event("ChainCommandRunner").Complete, this);
                    }
                    if(doDestroy) this.destroy();
                }
            }else{

                if(current!=null){

                    current.removeEventListener(mason.event("Command").Complete, this.run, this);
                    if(doDestroy)current.destroy();
                }

                current = chain[count];
                count ++;
                current.addEventListener(mason.event("Command").Complete, this.run, this);
                current.execute();


            }
        }
    }
}).extend("EventDispatcher");