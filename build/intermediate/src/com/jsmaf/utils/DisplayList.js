/**
 * User: bootstrap
 * Date: 10/26/13
 * Time: 5:24 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.DisplayList = {

   DisplayList:DisplayList = function(){

       this.pkm = null;
       this.list = null;

       DisplayList.prototype.view = function(__view){


           var view = document.getElementById(__view)
           var children = view.getElementsByTagName('*');

           var i=0;
           var l=children.length;


           for(i;i<l;++i){

               var item = children[i];
               var className = item.className;
               var id = item.id;


               if(className.indexOf("jsmaf-displayobject")>-1){

                   var dpo = this.pkm.create("DisplayObject",id);
                   view[id] = dpo;
               }

               if(className.indexOf("jsmaf-displayobjectinteractive")>-1){

                   var dpo = this.pkm.create("InteractiveDisplayObject",id);
                   view[id] = dpo;
               }
           }

           return view;
       }
   }
}