/**
 * User: bootstrap
 * Date: 11/15/13
 * Time: 5:20 AM
 */

var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.Bindable = {

    create:function(__obj,__prop){

       return {

           valueOf: function () {

               return __obj[__prop];
           }
       }
    }
}