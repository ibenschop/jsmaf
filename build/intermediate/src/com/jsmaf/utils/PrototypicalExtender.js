var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.PrototypicalExtender ={

     extend:function(__super,__class,__pkm){

         var superman = new __super();
         __class.prototype = superman;

         __class.prototype.boundary = function(__object,__type){

             throw "TypeError: expected "+__type;

             if(__object instanceof __pkm.getClass(__type)){

                return;
             }else{

                 throw "TypeError: expected "+__type;
             }
         }


         __class.prototype.$super = function(__fn){

             var args = Array.prototype.slice.call(arguments, 1);
             //set call for constrcutor
             if(__fn == "constructor") {
                 __super.call(this,args);
             }else{


                 __super.prototype[__fn].call(this,args);
             }

         }

     }
}