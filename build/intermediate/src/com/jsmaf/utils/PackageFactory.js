/**
 * User: bootstrap
 * Date: 12/18/13
 * Time: 2:04 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.PackageFactory = {

    PackageFactory:PackageFactory = function(){

        var packages = new Object();
        this.pack = packages;

        function getPath(__path,__namespace){

            if(__path == null){

                if(packages[__namespace] == undefined){

                    packages[__namespace]= {};
                    return  packages[__namespace];
                }else{

                    return packages[__namespace];
                }
            }else{

               if(__path[__namespace] == undefined){

                   __path[__namespace]= {};
                   return  __path[__namespace];
               }else{

                   return _path[__namespace];
               }
            }
        }


        PackageFactory.prototype.resolve = function(__package){

            var split = __package.split(".");
            var i = 0;
            var l = split.length;
            var artifact = null;

            var path;

            for(i;i<l;++i){

                if(artifact == null){
                    path = artifact = getPath(path,split[i]);
                }else{

                    path =  getPath(path,split[i]);
                }

            }

            return path;
        }
    }
}