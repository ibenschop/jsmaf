/**
 * User: bootstrap
 * Date: 10/28/13
 * Time: 7:48 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.EventManager = {

    EventManager:EventManager = function(__dispatcher){

        EventManager.prototype.add = function(__event,__callback,__target){

            this.dispatcher.addEventListener(__event,__callback,__target) ;
        }

        EventManager.prototype.remove = function(__event,__callback,__target){

           this.dispatcher.removeEventListener(__event,__callback,__target) ;
        }

    }
}

