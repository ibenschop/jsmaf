/**
 * User: bootstrap
 * Date: 11/13/13
 * Time: 12:34 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.ObjectDecoration = {

    GetterSetter:function(){

        //legacy support for getter and setter to be implemented (es5 emulation)
        //http://blogs.msdn.com/b/ie/archive/2010/09/07/transitioning-existing-code-to-the-es5-getter-setter-apis.aspx
        if (Object.prototype.__defineGetter__&&!Object.defineProperty) {
            Object.defineProperty=function(obj,prop,desc) {
                if ("get" in desc) obj.__defineGetter__(prop,desc.get);
                if ("set" in desc) obj.__defineSetter__(prop,desc.set);
            }
        }

    },
    getElementsByClassName:function(){

    if (document.getElementsByClassName == undefined) {
        document.getElementsByClassName = function(className)
        {
            var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
            var allElements = document.getElementsByTagName("*");
            var results = [];

            var element;
            for (var i = 0; (element = allElements[i]) != null; i++) {
                var elementClass = element.className;
                if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
                    results.push(element);
            }

            return results;
        }
    }
}
}