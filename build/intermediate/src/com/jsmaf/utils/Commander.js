var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.Commander = {

    Events:{
        Complete:"Complete"

    },

    Commander:Commander = function () {

        this.eventManager = null;
        this.pkm = null;
        this.mapping = [];
        this.chaining = {};
        this.currentChain = "";


        function onCommnandComplete(e) {

            if (e.target.chainID != "") {

                var id = e.target.chainID;

                if (this.chaining[id] != undefined) {

                    if (this.currentChain == id || this.currentChain == "") {

                        var count = this.chaining[id].count;

                        if (count <= 0) {

                            this.currentChain = id;
                            var command = this.chaining[id].list[count];
                            count++;
                            this.chaining[id].count = count;
                            command.execute();


                        } else {

                            this.chaining[id].count = 0;
                            this.currentChain = "";

                        }
                    }
                }
            }
        }


        Commander.prototype.map = function (__type, __event, __target, __props, __autoUnMap) {

            var command = this.pkm.command(__type, __target, __props);

            this.mapping.push({type:__type, event:__event, command:command});

            if (__event != null) this.eventManager.add(__event, command.execute, command);

            command.addEventListener(this.pkm.event("Command").Complete, onCommnandComplete, this);

            return command;
        }

        Commander.prototype.unmap = function (__type, __event, __target) {

            var i = 0;
            var l = this.mapping.length;
            if (l < 0)return;

            for (i; i < l; ++i) {

                var item = this.mapping[i];

                if (item.type == __type && item.event == __event) {
                    this.eventManager.remove(item.event, item.command.execute, item.command)
                    this.mapping.splice(i, 1);

                }
            }

        }

        Commander.prototype.chain = function (__initiator, __chain) {

            var command = this.map(__initiator.command, __initiator.event, __initiator.target, __initiator.props);
            var chainID = Math.random().toString(36).substring(10);
            command.chainID = chainID

            if (this.chaining[chainID] == undefined) {

                this.chaining[chainID] = {count:0, list:[]};
                var i = 0;
                var l = __chain.length;

                for (i; i < l; i++) {

                    var item = __chain[i];
                    var command = this.map(item.command, item.event, item.target, item.props);
                    command.chainID = chainID;
                    this.chaining[chainID].list.push(command)

                }
            } else {

                return;
            }
            //console.log(__initiator.command)
        }

        Commander.prototype.removeChain = function(__id){


        }
    }
}