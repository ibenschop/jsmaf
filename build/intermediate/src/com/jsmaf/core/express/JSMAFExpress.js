/**
 * User: bootstrap
 * Date: 12/18/13
 * Time: 7:05 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.express = com.jsmaf.core.express || {};


com.jsmaf.core.express.JSMAFExpress = {

    JSMAFExpress:JSMAFExpress = function(){

        var artifact = null;
        var entity = null;
        var Qdependencies = {};

        function onDOMContentLoaded(e){

          getDependencies();
        }


        function getDependencies(){



            for(var i in Qdependencies){

                var dep = Qdependencies[i];

                console.log(dep)
                for(var j=0;j<dep.length;++j){
                    console.log("7")
                    dep[j]();
                    dep.splice(j, 1);
                }


            }



            if(Qdependencies.length>=0){

                document.removeEventListener("DOMContentLoaded",onDOMContentLoaded);
            }
        }

        function init(){


            depend(["com.jsmaf.utils.PackageManager.PackageManager",
            "com.jsmaf.utils.PackageFactory"],run);
            document.addEventListener("DOMContentLoaded",onDOMContentLoaded);
            getDependencies();
        }

        var pkm,dispatcher,pcf,context,eventManager,commander;

        function run(){

            //create PackageManager and add packages
            pkm =  new com.jsmaf.utils.PackageManager.PackageManager();
            pkm.add(com.jsmaf.core.object);
            pkm.add(com.jsmaf.core.dispatcher);
            pkm.add(com.jsmaf.core.command);
            pkm.add(com.jsmaf.core.context);
            pkm.add(com.jsmaf.core.module);
            pkm.add(com.jsmaf.utils);
            pkm.add(com.jsmaf.core.display);

            //create dispatcher
            dispatcher = pkm.create("EventDispatcher");

            //create PackageFactory
            pcf = pkm.create("PackageFactory",window);

            //create the Context
            context = pkm.create("BasicContext");
            context.dispatcher = dispatcher;
            context.pkm = pkm;

            //create the EventManager
            eventManager = pkm.create("EventManager");
            eventManager.dispatcher = dispatcher;

            //create the Commander
            commander = pkm.create("Commander");
            commander.eventManager = eventManager;
            commander.pkm = pkm;

            console.log("1")

        }


        function resolveDependency(__dependency){


            function getPath(__path,__namespace){

                if(__path == null){

                    if(window[__namespace] == undefined){

                       return false;
                    }else{

                        return window[__namespace];
                    }
                }else{

                    if(__path[__namespace] == undefined){


                        return  false;
                    }else{

                        return __path[__namespace];
                    }
                }
            }


            var split = __dependency.split(".");
            var i = 0;
            var l = split.length;
            var parent = null;
            var path = null;

            for(i;i<l;++i){

                if(!getPath(path,split[i]))return false;

                if(artifact == null){
                    path = parent = getPath(path,split[i]);
                }else{

                    path =  getPath(path,split[i]);
                }

            }

            return true;
        }

        JSMAFExpress.prototype.pkg = function(__namespace){

            console.log("2")
            artifact =  pcf.resolve(__namespace);
            return this;
        }

        JSMAFExpress.prototype.module = function(__module){

            for(var i in __module){

                artifact[i] = __module;
                pkm.addClass(i,__module);
                entity = i;
            }

            artifact = null;

           return this;
        }

        JSMAFExpress.prototype.addModule = function(__module){

            var module = pkm.create(__module);
            context.addModule(module,__module);

            return module;
        }

        JSMAFExpress.prototype.removeModule = function(__module){


        }

        JSMAFExpress.prototype.extend = function(__super){

            pkm.util("PrototypicalExtender").extend(pkm.getClass(__super),pkm.getClass(entity));
        }

        JSMAFExpress.prototype.startup = function(){

            context.startup();
        }

        JSMAFExpress.prototype.depend = function(__dependencies,__callback){

            depend(__dependencies,__callback);
        }


        function depend (__dependencies,__callback){


            var i = 0;
            var l = __dependencies.length;
            var item;

            for(i ;i<l;++i){

                item = __dependencies[i];
                console.log("1")
                if(!resolveDependency(item)){
                    console.log("2")
                   if(Qdependencies[item] == undefined){

                       Qdependencies[item] = [];
                       Qdependencies[item].push(__callback);
                       console.log("3")

                   }else{

                       Qdependencies[item].push(__callback);
                       console.log("4")
                   }
                }else{

                    __callback();
                }

            }

            getDependencies();
        }

        init();
    }
};

(function(window){


    window.jsmafe = new com.jsmaf.core.express.JSMAFExpress.JSMAFExpress();


})(window);
