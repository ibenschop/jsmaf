var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.dispatcher = com.jsmaf.core.dispatcher || {}

com.jsmaf.core.dispatcher.EventDispatcher = {


    EventDispatcher:EventDispatcher = function () {

        //privates
         this.events = [];

        //delegater
        function delegate(__scope, __callback) {

            return function (arg) {

                __callback.call(__scope,arg);
            }
        }


        //addEventListener
        EventDispatcher.prototype.addEventListener = function (__event, __callback, __target) {


            if ( this.events[__event] == undefined) {

                this.events[__event] = [];
                this.events[__event].push({target:__target, callback:delegate(__target, __callback)});
            } else {

                var event =  this.events[__event];

                var i = 0, l = event.length;

                for (i; i < l; ++i) {

                    var item = event[i];

                    if (item.target != __target) {

                        this.events[__event].push({target:__target, callback:delegate(__target, __callback)});
                        return;
                    } else {

                        return;
                    }
                }
            }
        }


        //removeEventListener
        EventDispatcher.prototype.removeEventListener = function (__event, __callback, __target) {

            if ( this.events[__event]) {

                var listeners =  this.events[__event], i = 0, l = listeners.length;

                for (i; i < l; ++i) {

                    var item = listeners[i]

                    if ((item.target == __target)) {

                        listeners.splice(i, 1);


                        if (listeners.length == 0) {

                            this.events[__event] = null;
                            delete   this.events[__event];
                        }
                        return true;
                    }
                }
            }

            return false;

        }


        //dispatchEvent
        EventDispatcher.prototype.dispatchEvent = function (__event, __target, __payload) {

            var listeners =  this.events[__event];

            if (listeners == undefined)return;
            if (listeners.length <= 0)return;

            var l = listeners.length, i = 0;

            for (i; i < l; ++i) {

                var item = listeners[i]

                item.callback({event:__event, target:__target, payload:__payload});
            }

        }
    }
}








