mason.pkg("com.jsmaf.core.module").module("BasicModel",{

     Events:{

      Update:"BasicModel.Update"
     },

      Class:function(){

         this.method.update = function(){

              if(this.dispatcher!=null) this.dispatcher.dispatchEvent(mason.event("BasicModel").Update, this, null);

          }
      }
}).extend("BasicModule");