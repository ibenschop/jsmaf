/**
 * User: bootstrap
 * Date: 10/25/13
 * Time: 6:08 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.module = com.jsmaf.core.module || {}

com.jsmaf.core.module.ViewModule = {

     ViewModule:ViewModule = function(__view){


         this.view = null;


         ViewModule.prototype.setView = function(__view){

            this.view = __view;
             return this;
         }

         ViewModule.prototype.startup = function(){

             //this.view.box1Somebox.position = "absolute";
             //this.view.box1Somebox.x = "20%";
             this.bindEvents();
             return this;
         }


         ViewModule.prototype.show = function(){

             return this;
         }

         ViewModule.prototype.hide = function(){

             return this;
         }

         ViewModule.prototype.bindEvents = function(){

             return this;
         }

         ViewModule.prototype.unBindEvents = function(){

             return this;
         }

         return this;
     }
}

