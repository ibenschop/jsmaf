/**
 * User: bootstrap
 * Date: 11/20/13
 * Time: 6:33 PM
 */
/**
 * User: bootstrap
 * Date: 11/15/13
 * Time: 11:50 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.display = com.jsmaf.core.display || {}

com.jsmaf.core.display.InteractiveDisplayObject = {

    InteractiveDisplayObject:InteractiveDisplayObject = function(__what){

        this.$super("constructor",__what);

        this.cursor = "pointer";

        Object.defineProperties(this, {
            "click": {
                "get": function() { return  this.element.onclick},
                "set": function(__value) {

                    this.element.onclick = null;
                    this.element.onclick = __value;
                }
            },
            "doubleclick": {
                "get": function() { return  this.element.ondblclick},
                "set": function(__value) {

                    this.element.ondblclick = null;
                    this.element.ondblclick = __value;
                }
            },
            "onmousedown": {
                "get": function() { return  this.element.onmousedown},
                "set": function(__value) {

                    this.element.onmousedown = null;
                    this.element.onmousedown = __value;
                }
            },
            "onmousemove": {
                "get": function() { return  this.element.onmousemove},
                "set": function(__value) {

                    this.element.onmousemove = null;
                    this.element.onmousemove = __value;
                }
            },
            "onmouseover": {
                "get": function() { return  this.element.onmouseover},
                "set": function(__value) {

                    this.element.onmouseover = null;
                    this.element.onmouseover = __value;
                }
            },
            "onmouseout": {
                "get": function() { return  this.element.onmouseout},
                "set": function(__value) {

                    this.element.onmouseout = null;
                    this.element.onmouseout = __value;
                }
            },
            "onmouseup": {
                "get": function() { return  this.element.onmouseup},
                "set": function(__value) {

                    this.element.onmouseup = null;
                    this.element.onmouseup = __value;
                }
            }
        });
    }
}