var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.command = com.jsmaf.core.command || {}

com.jsmaf.core.command.Command = {

    Events:{
        Complete:"Complete"

    },

    Command:Command = function () {

        this.target = null;
        this.chainID = "";
        var namespace = com.jsmaf.core.command.Command;

        function validate(){

            if(this.target!=null || this.target!= undefined)return false;
        }

        Command.prototype.execute = function () {

             if(!validate())return;


        };

        Command.prototype.destroy = function(){

            this.chainID = null;
            this.target = null
        }

        Command.prototype.undo = function () {

            if(!validate())return;
        }

        Command.prototype.onComplete = function(){


            if(this.dispatchEvent!=undefined){

                this.dispatchEvent(namespace.Events.Complete,this,null);
            }
        }

    }
}