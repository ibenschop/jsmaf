mason.pkg("com.jsmaf.polyfills").util("PollyFillGetterSetter",{

    PollyFillGetterSetter:function(){

        //legacy support for getter and setter to be implemented (es5 emulation)
        //http://blogs.msdn.com/b/ie/archive/2010/09/07/transitioning-existing-code-to-the-es5-getter-setter-apis.aspx
        if (Object.prototype.__defineGetter__&&!Object.defineProperty) {
            Object.defineProperty=function(obj,prop,desc) {
                if ("get" in desc) obj.__defineGetter__(prop,desc.get);
                if ("set" in desc) obj.__defineSetter__(prop,desc.set);
            }
        }

    }
});