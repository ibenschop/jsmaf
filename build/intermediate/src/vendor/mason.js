/**
 * User: bootstrap
 * Date: 12/23/13
 * Time: 7:08 PM
 */

(function (window) {

    var Mason = function () {

        var classes = {};
        var packages = {};
        var shift = [].shift;
        var slice = [].slice;
        var events = {};
        var utils = {};
        var currentClass = null;
        var currentClassName = ""
        var queedExtenstion = {};
        var instances = {};
        var version = "0.0.1";

        function extend(__super) {

            if (classes[__super] == undefined) {

                if (queedExtenstion[__super] == undefined) {

                    queedExtenstion[__super] = [];
                    queedExtenstion[__super].push(currentClassName);
                } else {

                    queedExtenstion[__super].push(currentClassName);
                }

            } else {


                var $super = classes[__super];


                var superman = new $super();
                currentClass.prototype = superman;
                currentClass.prototype.method = currentClass.prototype;
                currentClass.prototype.$super = function (__fn) {

                    var args = slice.call(arguments, 1);

                    if (__fn == "constructor") {
                        $super.call(this, args);
                    } else {

                        currentClass.prototype[__fn].call(this, args);
                    }
                }
            }
        }

        function getPath(__path, __namespace) {

            if (__path == null) {

                if (packages[__namespace] == undefined) {

                    packages[__namespace] = {};
                    return  packages[__namespace];
                } else {

                    return packages[__namespace];
                }
            } else {

                if (__path[__namespace] == undefined) {

                    __path[__namespace] = {};
                    return  __path[__namespace];
                } else {

                    return __path[__namespace];
                }
            }
        }

        function pkg(__package) {

            var split = __package.split(".");
            var i = 0;
            var l = split.length;
            var artifact = null;

            var path = null;

            for (i; i < l; ++i) {

                if (artifact == null) {

                    path = artifact = getPath(path, split[i]);
                } else {

                    path = getPath(path, split[i]);
                }

            }
            return this;
        }


        //TODO make package depended
        function addPackage(__package) {

            for (var i in __package) {

                if (i != "Events")this.classes[i] = __package[i];

            }
        }

        function module(__module, __class) {

            for (var i in __class) {

                if(i!= "Events" && i!="Class" && i!="Uses"){

                    throw new Error("Unexpected Package Property found in Class: "+__module+", Property: "+i);
                }
            }
            if(__class.Class == undefined){

                throw new Error("Class Property not found in Class: "+__module);
            }else{

                classes[__module] = __class.Class;
            }

            events[__module] = __class.Events
            currentClass = __class.Class;
            currentClass.prototype.method  = currentClass.prototype;
            currentClassName = __module;

            return this;
        }

        //TODO make package depended
        function create(__class,__tag) {

            try {
                var result = classes[__class];
                var clsClass = classes[__class];
                var instance = null

                if (arguments.length > 1) {

                    var args = Array.prototype.slice.call(arguments, 1);

                    function builder() {
                        return clsClass.apply(this, args);
                    }

                    builder.prototype = clsClass.prototype;
                    instance = new builder();

                } else {

                    instance = new clsClass();
                    instance.tag = __class;
                }


            } catch (e) {


                throw new Error("Class not Found, Class: " + __class);
                //class is not available yet
            }

            if (instances[__tag] == undefined) {

                instances[__tag] = instance;
            }

            return instance;

        }

        function inject(__class) {

            if (instances[__class] != undefined) {

                return  instances[__class];
            } else{

                throw new Error("Class istance not Found with TAG, TAG: " + __class);
            }

        }

        function getClass(__class) {

            try {

                var result = classes[__class][__class];
            } catch (e) {

                if (e.name == "TypeError")throw new Error("TypeError", "Class of type " + __class + " is unknown");

            }

            return result;
        }

        function util(__util, __object) {

            if (__object.Class != undefined) {

                module(__util, __object);
                return this;

            }

            if (utils[__util] != undefined) {

                return utils[__util];
            } else {

                utils[__util] = __object;
            }
        }

        function event(__class) {

            return events[__class];
        }

        function command(__command, __target, __props) {
            try {

                var cls = classes[__command];
                var instance = new cls(__props);
                instance.target = __target;
                return instance;
            } catch (e) {

                console.trace();
                throw new Error("Class not Found, Class: " + __command);

            }
        }


        function validateScriptLoaded(__package) {

            var split = __package.split(".");

            var result = false;
            if (classes[split[split.length - 1]]) {

                result = true;
                if (queedExtenstion[split[split.length - 1]] != undefined) {

                    var q = queedExtenstion[split[split.length - 1]];
                    for (var i = 0; i < q.length; ++i) {

                        var item = q[i];
                        currentClass = classes[item];

                        extend(split[split.length - 1]);
                        delete queedExtenstion[currentClassName];
                    }
                }
            }
            if (utils[split[split.length - 1]])result = true;

            return result;
        }

        function includer(__prefix, __scripts, __callback) {

            var src;
            var prefix = __prefix
            var scripts = __scripts;
            var pendingScripts = [];
            var firstScript = document.scripts[0];


            function load() {

                var l = scripts.length;
                var count = 0;

                while (src = scripts.shift()) {

                    //check if its already available
                    if (validateScriptLoaded(src)) {

                        count++;
                        //its not available try to load it
                    } else {


                        var fullSrc = prefix + "/" + src.replace(/[.]/g, "/") + ".js?"
                        script = document.createElement('script');
                        script.type = "text/javascript";
                        script.src = fullSrc;
                        script.classPath = src;

                        script.onload = function (e) {

                            if (validateScriptLoaded(this.classPath)) {

                                count++;
                                if (count >= l) {


                                    __callback();
                                }
                            }
                        }

                        if ('async' in firstScript) { // modern browsers

                            script.async = false;

                            document.head.appendChild(script);
                        }
                        else if (firstScript.readyState) { // IE<10

                            pendingScripts.push(script);
                            script.onreadystatechange = stateChange;

                        }
                        else {
                            document.write('<script src="' + fullSrc + '" defer></' + 'script>');
                        }
                    }
                }
            }

            return{

                obj:this,
                load:load
            }

        }

        return{
            obj:this,
            command:command,
            create:create,
            addPackage:addPackage,
            util:util,
            event:event,
            module:module,
            pkg:pkg,
            extend:extend,
            include:includer,
            inject:inject,
            version:version
        }
    }

    window.mason = new Mason();
})(window);
