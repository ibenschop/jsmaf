var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.command = com.jsmaf.core.command || {}

com.jsmaf.core.command.Command = {

    Events:{
        Complete:"Complete"

    },

    Command:Command = function () {

        this.target = null;
        this.chainID = "";
        var namespace = com.jsmaf.core.command.Command;

        function validate(){

            if(this.target!=null || this.target!= undefined)return false;
        }

        Command.prototype.execute = function () {

             if(!validate())return;


        };

        Command.prototype.destroy = function(){

            this.chainID = null;
            this.target = null
        }

        Command.prototype.undo = function () {

            if(!validate())return;
        }

        Command.prototype.onComplete = function(){


            if(this.dispatchEvent!=undefined){

                this.dispatchEvent(namespace.Events.Complete,this,null);
            }
        }

    }
}
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.context = com.jsmaf.core.context || {}

com.jsmaf.core.context.BasicContext = {

    Events:{

        Startup:"Startup"
    },

    BasicContext:BasicContext = function(){


        this.dispatcher = null;
        this.viewport = null;
        this.modules = [];
        this.pkm = null;
        this.namespace = com.jsmaf.core.context.BasicContext


        BasicContext.prototype.addModule = function (__module,__name,__startup) {


            this.modules.push({module:__module,name:__name,startup:__startup || true});
            __module.register(this);
        }

        BasicContext.prototype.getModule = function(__name){

            var i = 0;
            var l = this.modules.length

            for(i; i<l; ++i){

                var item = this.modules[i];
                if(item.name === __name){

                    return item.module;
                    break;
                }
            }
        }

        BasicContext.prototype.startup = function () {

            var i = 0;
            var l = this.modules.length;

            for(i; i<l; ++i){

                var item = this.modules[i];
                if(item.startup == true)item.module.startup();
            }
            if(this.dispatcher!=null) this.dispatcher.dispatchEvent(this.namespace.Events.Startup, this, null);

        }
    }
}
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.dispatcher = com.jsmaf.core.dispatcher || {}

com.jsmaf.core.dispatcher.EventDispatcher = {


    EventDispatcher:EventDispatcher = function () {

        //privates
         this.events = [];

        //delegater
        function delegate(__scope, __callback) {

            return function (arg) {

                __callback.call(__scope,arg);
            }
        }


        //addEventListener
        EventDispatcher.prototype.addEventListener = function (__event, __callback, __target) {


            if ( this.events[__event] == undefined) {

                this.events[__event] = [];
                this.events[__event].push({target:__target, callback:delegate(__target, __callback)});
            } else {

                var event =  this.events[__event];

                var i = 0, l = event.length;

                for (i; i < l; ++i) {

                    var item = event[i];

                    if (item.target != __target) {

                        this.events[__event].push({target:__target, callback:delegate(__target, __callback)});
                        return;
                    } else {

                        return;
                    }
                }
            }
        }


        //removeEventListener
        EventDispatcher.prototype.removeEventListener = function (__event, __callback, __target) {

            if ( this.events[__event]) {

                var listeners =  this.events[__event], i = 0, l = listeners.length;

                for (i; i < l; ++i) {

                    var item = listeners[i]

                    if ((item.target == __target)) {

                        listeners.splice(i, 1);


                        if (listeners.length == 0) {

                            this.events[__event] = null;
                            delete   this.events[__event];
                        }
                        return true;
                    }
                }
            }

            return false;

        }


        //dispatchEvent
        EventDispatcher.prototype.dispatchEvent = function (__event, __target, __payload) {

            var listeners =  this.events[__event];

            if (listeners == undefined)return;
            if (listeners.length <= 0)return;

            var l = listeners.length, i = 0;

            for (i; i < l; ++i) {

                var item = listeners[i]

                item.callback({event:__event, target:__target, payload:__payload});
            }

        }
    }
}









/**
 * User: bootstrap
 * Date: 11/10/13
 * Time: 2:18 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.display = com.jsmaf.core.display || {}

com.jsmaf.core.display.DisplayObject ={

    DisplayObject:DisplayObject = function(__what) {


        this.element = document.getElementById(__what);

        Object.defineProperties(this, {
            "x": {
                "get": function() { return  this.element.style.left;},
                "set": function(__value) { this.element.style.left = __value; }
            },
            "alpha": {
                "get":  function(){
                    return this.element.style.opacity;
                },
                "set": function(__value){

                    this.element.style.opacity = __value;
                }
            },
            "position": {
                "get":  function(){

                    return this.element.style.position
                },
                "set":  function(__value){

                    this.element.style.position = __value;
                }
            },
            "height": {
                "get":  function(){

                    return this.element.style.height
                },
                "set":  function(__value){

                    this.element.style.height = __value;
                }
            },
            "width": {
                "get":  function(){

                    return this.element.style.width
                },
                "set":  function(__value){

                    this.element.style.width = __value;
                }
            },
            "display": {
                "get":  function(){

                    return this.element.style.display
                },
                "set":  function(__value){

                    this.element.style.display = __value;
                }
            },
            "cursor": {
                "get":  function(){

                    return this.element.style.cursor
                },
                "set":  function(__value){

                    this.element.style.cursor = __value;
                }
            }
        });
    }
}
/**
 * User: bootstrap
 * Date: 11/15/13
 * Time: 11:50 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.display = com.jsmaf.core.display || {}

com.jsmaf.core.display.DisplayObjectContainer = {

    DisplayObjectContainer:DisplayObjectContainer = function(__what){

        this.$super("constructor",__what);


        this.children = {}

        DisplayObjectContainer.prototype.addChild = function(__child){


            var child = {};
            return;
            if(__child.id== ""){

                var id =  Math.random().toString(36).substring(10);
                __child.id = id;
                child.id = id;

            }
            child.el =this.element.appendChild(__child);

            return child;
        }

        DisplayObjectContainer.prototype.removeChild = function(__child){

            var element = document.getElementById(__child);
            this.element.removeChild(element);
        }

        DisplayObjectContainer.prototype.addChildAt = function(__child,__at){

            this.element.insertBefore(__child, this.element.children[__at]);
        }

        DisplayObjectContainer.prototype.removeChildAt = function(__at){

            var element = this.element.children[__at];
            this.element.removeChild(element);
        }
    }
}
/**
 * User: bootstrap
 * Date: 11/20/13
 * Time: 6:33 PM
 */
/**
 * User: bootstrap
 * Date: 11/15/13
 * Time: 11:50 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.display = com.jsmaf.core.display || {}

com.jsmaf.core.display.InteractiveDisplayObject = {

    InteractiveDisplayObject:InteractiveDisplayObject = function(__what){

        this.$super("constructor",__what);

        this.cursor = "pointer";

        Object.defineProperties(this, {
            "click": {
                "get": function() { return  this.element.onclick},
                "set": function(__value) {

                    this.element.onclick = null;
                    this.element.onclick = __value;
                }
            },
            "doubleclick": {
                "get": function() { return  this.element.ondblclick},
                "set": function(__value) {

                    this.element.ondblclick = null;
                    this.element.ondblclick = __value;
                }
            },
            "onmousedown": {
                "get": function() { return  this.element.onmousedown},
                "set": function(__value) {

                    this.element.onmousedown = null;
                    this.element.onmousedown = __value;
                }
            },
            "onmousemove": {
                "get": function() { return  this.element.onmousemove},
                "set": function(__value) {

                    this.element.onmousemove = null;
                    this.element.onmousemove = __value;
                }
            },
            "onmouseover": {
                "get": function() { return  this.element.onmouseover},
                "set": function(__value) {

                    this.element.onmouseover = null;
                    this.element.onmouseover = __value;
                }
            },
            "onmouseout": {
                "get": function() { return  this.element.onmouseout},
                "set": function(__value) {

                    this.element.onmouseout = null;
                    this.element.onmouseout = __value;
                }
            },
            "onmouseup": {
                "get": function() { return  this.element.onmouseup},
                "set": function(__value) {

                    this.element.onmouseup = null;
                    this.element.onmouseup = __value;
                }
            }
        });
    }
}
/**
 * User: bootstrap
 * Date: 12/18/13
 * Time: 7:05 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.express = com.jsmaf.core.express || {};


com.jsmaf.core.express.JSMAFExpress = {

    JSMAFExpress:JSMAFExpress = function(){

        var artifact = null;
        var entity = null;
        var Qdependencies = {};

        function onDOMContentLoaded(e){

          getDependencies();
        }


        function getDependencies(){



            for(var i in Qdependencies){

                var dep = Qdependencies[i];

                console.log(dep)
                for(var j=0;j<dep.length;++j){
                    console.log("7")
                    dep[j]();
                    dep.splice(j, 1);
                }


            }



            if(Qdependencies.length>=0){

                document.removeEventListener("DOMContentLoaded",onDOMContentLoaded);
            }
        }

        function init(){


            depend(["com.jsmaf.utils.PackageManager.PackageManager",
            "com.jsmaf.utils.PackageFactory"],run);
            document.addEventListener("DOMContentLoaded",onDOMContentLoaded);
            getDependencies();
        }

        var pkm,dispatcher,pcf,context,eventManager,commander;

        function run(){

            //create PackageManager and add packages
            pkm =  new com.jsmaf.utils.PackageManager.PackageManager();
            pkm.add(com.jsmaf.core.object);
            pkm.add(com.jsmaf.core.dispatcher);
            pkm.add(com.jsmaf.core.command);
            pkm.add(com.jsmaf.core.context);
            pkm.add(com.jsmaf.core.module);
            pkm.add(com.jsmaf.utils);
            pkm.add(com.jsmaf.core.display);

            //create dispatcher
            dispatcher = pkm.create("EventDispatcher");

            //create PackageFactory
            pcf = pkm.create("PackageFactory",window);

            //create the Context
            context = pkm.create("BasicContext");
            context.dispatcher = dispatcher;
            context.pkm = pkm;

            //create the EventManager
            eventManager = pkm.create("EventManager");
            eventManager.dispatcher = dispatcher;

            //create the Commander
            commander = pkm.create("Commander");
            commander.eventManager = eventManager;
            commander.pkm = pkm;

            console.log("1")

        }


        function resolveDependency(__dependency){


            function getPath(__path,__namespace){

                if(__path == null){

                    if(window[__namespace] == undefined){

                       return false;
                    }else{

                        return window[__namespace];
                    }
                }else{

                    if(__path[__namespace] == undefined){


                        return  false;
                    }else{

                        return __path[__namespace];
                    }
                }
            }


            var split = __dependency.split(".");
            var i = 0;
            var l = split.length;
            var parent = null;
            var path = null;

            for(i;i<l;++i){

                if(!getPath(path,split[i]))return false;

                if(artifact == null){
                    path = parent = getPath(path,split[i]);
                }else{

                    path =  getPath(path,split[i]);
                }

            }

            return true;
        }

        JSMAFExpress.prototype.pkg = function(__namespace){

            console.log("2")
            artifact =  pcf.resolve(__namespace);
            return this;
        }

        JSMAFExpress.prototype.module = function(__module){

            for(var i in __module){

                artifact[i] = __module;
                pkm.addClass(i,__module);
                entity = i;
            }

            artifact = null;

           return this;
        }

        JSMAFExpress.prototype.addModule = function(__module){

            var module = pkm.create(__module);
            context.addModule(module,__module);

            return module;
        }

        JSMAFExpress.prototype.removeModule = function(__module){


        }

        JSMAFExpress.prototype.extend = function(__super){

            pkm.util("PrototypicalExtender").extend(pkm.getClass(__super),pkm.getClass(entity));
        }

        JSMAFExpress.prototype.startup = function(){

            context.startup();
        }

        JSMAFExpress.prototype.depend = function(__dependencies,__callback){

            depend(__dependencies,__callback);
        }


        function depend (__dependencies,__callback){


            var i = 0;
            var l = __dependencies.length;
            var item;

            for(i ;i<l;++i){

                item = __dependencies[i];
                console.log("1")
                if(!resolveDependency(item)){
                    console.log("2")
                   if(Qdependencies[item] == undefined){

                       Qdependencies[item] = [];
                       Qdependencies[item].push(__callback);
                       console.log("3")

                   }else{

                       Qdependencies[item].push(__callback);
                       console.log("4")
                   }
                }else{

                    __callback();
                }

            }

            getDependencies();
        }

        init();
    }
};

(function(window){


    window.jsmafe = new com.jsmaf.core.express.JSMAFExpress.JSMAFExpress();


})(window);

var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.module = com.jsmaf.core.module || {}

com.jsmaf.core.module.BasicModule = {

    BasicModule:BasicModule = function () {

        this.context = null;
        this.dispatcher = null;
        this.pkm = null;
        this.tag =  "BasicModule"


        BasicModule.prototype.register = function(__context)
        {
            if(__context!=undefined){

                this.context = __context;
                this.dispatcher = this.context.dispatcher;
                this.pkm = this.context.pkm;

            }else{

                return;
            }
        }

        BasicModule.prototype.startup = function () {

            console.log("startup")
        }

        return this;
    }
}



/**
 * User: bootstrap
 * Date: 10/25/13
 * Time: 6:08 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.module = com.jsmaf.core.module || {}

com.jsmaf.core.module.ViewModule = {

     ViewModule:ViewModule = function(__view){


         this.view = null;


         ViewModule.prototype.setView = function(__view){

            this.view = __view;
             return this;
         }

         ViewModule.prototype.startup = function(){

             //this.view.box1Somebox.position = "absolute";
             //this.view.box1Somebox.x = "20%";
             this.bindEvents();
             return this;
         }


         ViewModule.prototype.show = function(){

             return this;
         }

         ViewModule.prototype.hide = function(){

             return this;
         }

         ViewModule.prototype.bindEvents = function(){

             return this;
         }

         ViewModule.prototype.unBindEvents = function(){

             return this;
         }

         return this;
     }
}


/**
 * User: bootstrap
 * Date: 11/21/13
 * Time: 6:28 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.object = com.jsmaf.core.object || {}


com.jsmaf.core.object.CommandObject = {


    CommandObject:CommandObject = function(__command,__event,__target,__props){

        this.command = __command;
        this.event = __event;
        this.target = __target;
        this.props = __props;
    }

}
/**
 * User: bootstrap
 * Date: 11/16/13
 * Time: 12:31 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.core = com.jsmaf.core || {};
com.jsmaf.core.object = com.jsmaf.core.object || {}

com.jsmaf.core.object.CoreObject = {

    CoreObject:CoreObject = function(){





    }

}
/**
 * User: bootstrap
 * Date: 11/15/13
 * Time: 5:20 AM
 */

var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.Bindable = {

    create:function(__obj,__prop){

       return {

           valueOf: function () {

               return __obj[__prop];
           }
       }
    }
}
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.Commander = {

    Events:{
        Complete:"Complete"

    },

    Commander:Commander = function () {

        this.eventManager = null;
        this.pkm = null;
        this.mapping = [];
        this.chaining = {};
        this.currentChain = "";


        function onCommnandComplete(e) {

            if (e.target.chainID != "") {

                var id = e.target.chainID;

                if (this.chaining[id] != undefined) {

                    if (this.currentChain == id || this.currentChain == "") {

                        var count = this.chaining[id].count;

                        if (count <= 0) {

                            this.currentChain = id;
                            var command = this.chaining[id].list[count];
                            count++;
                            this.chaining[id].count = count;
                            command.execute();


                        } else {

                            this.chaining[id].count = 0;
                            this.currentChain = "";

                        }
                    }
                }
            }
        }


        Commander.prototype.map = function (__type, __event, __target, __props, __autoUnMap) {

            var command = this.pkm.command(__type, __target, __props);

            this.mapping.push({type:__type, event:__event, command:command});

            if (__event != null) this.eventManager.add(__event, command.execute, command);

            command.addEventListener(this.pkm.event("Command").Complete, onCommnandComplete, this);

            return command;
        }

        Commander.prototype.unmap = function (__type, __event, __target) {

            var i = 0;
            var l = this.mapping.length;
            if (l < 0)return;

            for (i; i < l; ++i) {

                var item = this.mapping[i];

                if (item.type == __type && item.event == __event) {
                    this.eventManager.remove(item.event, item.command.execute, item.command)
                    this.mapping.splice(i, 1);

                }
            }

        }

        Commander.prototype.chain = function (__initiator, __chain) {

            var command = this.map(__initiator.command, __initiator.event, __initiator.target, __initiator.props);
            var chainID = Math.random().toString(36).substring(10);
            command.chainID = chainID

            if (this.chaining[chainID] == undefined) {

                this.chaining[chainID] = {count:0, list:[]};
                var i = 0;
                var l = __chain.length;

                for (i; i < l; i++) {

                    var item = __chain[i];
                    var command = this.map(item.command, item.event, item.target, item.props);
                    command.chainID = chainID;
                    this.chaining[chainID].list.push(command)

                }
            } else {

                return;
            }
            //console.log(__initiator.command)
        }

        Commander.prototype.removeChain = function(__id){


        }
    }
}
/**
 * User: bootstrap
 * Date: 10/26/13
 * Time: 5:24 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.DisplayList = {

   DisplayList:DisplayList = function(){

       this.pkm = null;
       this.list = null;

       DisplayList.prototype.view = function(__view){


           var view = document.getElementById(__view)
           var children = view.getElementsByTagName('*');

           var i=0;
           var l=children.length;


           for(i;i<l;++i){

               var item = children[i];
               var className = item.className;
               var id = item.id;


               if(className.indexOf("jsmaf-displayobject")>-1){

                   var dpo = this.pkm.create("DisplayObject",id);
                   view[id] = dpo;
               }

               if(className.indexOf("jsmaf-displayobjectinteractive")>-1){

                   var dpo = this.pkm.create("InteractiveDisplayObject",id);
                   view[id] = dpo;
               }
           }

           return view;
       }
   }
}
/**
 * User: bootstrap
 * Date: 10/28/13
 * Time: 7:48 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.EventManager = {

    EventManager:EventManager = function(__dispatcher){

        EventManager.prototype.add = function(__event,__callback,__target){

            this.dispatcher.addEventListener(__event,__callback,__target) ;
        }

        EventManager.prototype.remove = function(__event,__callback,__target){

           this.dispatcher.removeEventListener(__event,__callback,__target) ;
        }

    }
}


/**
 * User: bootstrap
 * Date: 11/15/13
 * Time: 5:20 AM
 */

var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.HtmlTags = {

    create:function(__type){


        switch(__type){

            case "div":
                return document.createElement('div');
                break;
                default:
                break

        }


    }

}
/**
 * User: bootstrap
 * Date: 11/13/13
 * Time: 12:34 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.ObjectDecoration = {

    GetterSetter:function(){

        //legacy support for getter and setter to be implemented (es5 emulation)
        //http://blogs.msdn.com/b/ie/archive/2010/09/07/transitioning-existing-code-to-the-es5-getter-setter-apis.aspx
        if (Object.prototype.__defineGetter__&&!Object.defineProperty) {
            Object.defineProperty=function(obj,prop,desc) {
                if ("get" in desc) obj.__defineGetter__(prop,desc.get);
                if ("set" in desc) obj.__defineSetter__(prop,desc.set);
            }
        }

    },
    getElementsByClassName:function(){

    if (document.getElementsByClassName == undefined) {
        document.getElementsByClassName = function(className)
        {
            var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
            var allElements = document.getElementsByTagName("*");
            var results = [];

            var element;
            for (var i = 0; (element = allElements[i]) != null; i++) {
                var elementClass = element.className;
                if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
                    results.push(element);
            }

            return results;
        }
    }
}
}
/**
 * User: bootstrap
 * Date: 12/18/13
 * Time: 2:04 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.PackageFactory = {

    PackageFactory:PackageFactory = function(){

        var packages = new Object();
        this.pack = packages;

        function getPath(__path,__namespace){

            if(__path == null){

                if(packages[__namespace] == undefined){

                    packages[__namespace]= {};
                    return  packages[__namespace];
                }else{

                    return packages[__namespace];
                }
            }else{

               if(__path[__namespace] == undefined){

                   __path[__namespace]= {};
                   return  __path[__namespace];
               }else{

                   return _path[__namespace];
               }
            }
        }


        PackageFactory.prototype.resolve = function(__package){

            var split = __package.split(".");
            var i = 0;
            var l = split.length;
            var artifact = null;

            var path;

            for(i;i<l;++i){

                if(artifact == null){
                    path = artifact = getPath(path,split[i]);
                }else{

                    path =  getPath(path,split[i]);
                }

            }

            return path;
        }
    }
}
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.PackageManager = {

    PackageManager:PackageManager = function(){

        this.classes = {};
        this.packages = {};
        var n = [].shift;

        PackageManager.prototype.add = function(__package){

            for(var i in __package){

                if(i != "Events")this.classes[i] = __package[i];

            }
        }

        PackageManager.prototype.addClass = function(__name,__class){

            this.classes[__name] = __class;
        }

        PackageManager.prototype.create = function (__class){


            var result = this.classes[__class][__class];
            var clsClass = this.classes[__class][__class];

            if(arguments.length>1){

                var args = Array.prototype.slice.call(arguments, 1);

                function builder() {
                    return clsClass.apply(this, args);
                }
                builder.prototype = clsClass.prototype;
                return new builder();
            } else{


                return new clsClass();
            }
            var args = Array.prototype.slice.call(arguments, 1);


            return new result(args);

        }

        PackageManager.prototype.getClass = function(__class){

            try{

                var result = this.classes[__class][__class];
            } catch(e){

                if(e.name == "TypeError")throw new Error("TypeError","Class of type "+__class+" is unknown");

            }

            return result;
        }

        PackageManager.prototype.util = function(__util){

            var result = this.classes[__util];
            return result;
        }

        PackageManager.prototype.event= function(__class){

            var result =this.classes[__class];
            return result.Events;
        }

        PackageManager.prototype.command = function(__command,__target,__props){

            var cls = this.classes[__command][__command];
            var instance = new cls(__props);
            instance.target = __target;
            return instance;
        }
    }
}



var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.PrototypicalExtender ={

     extend:function(__super,__class,__pkm){

         var superman = new __super();
         __class.prototype = superman;

         __class.prototype.boundary = function(__object,__type){

             throw "TypeError: expected "+__type;

             if(__object instanceof __pkm.getClass(__type)){

                return;
             }else{

                 throw "TypeError: expected "+__type;
             }
         }


         __class.prototype.$super = function(__fn){

             var args = Array.prototype.slice.call(arguments, 1);
             //set call for constrcutor
             if(__fn == "constructor") {
                 __super.call(this,args);
             }else{


                 __super.prototype[__fn].call(this,args);
             }

         }

     }
}
/**
 * User: bootstrap
 * Date: 11/16/13
 * Time: 4:55 AM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};

com.jsmaf.utils.StrictType = {

    StrictType:StrictType = function(){

        this.pkm = {}

        StrictType.prototype.pkm = function(__pkm){

              this.pkm = __pkm;
        }

        StrictType.prototype.check = function(__object,__type){

            if(__object instanceof this.pkm.getClass(__type)){

                console.log("YEAH");
            }
        }

    }

}

/**
 * User: bootstrap
 * Date: 11/22/13
 * Time: 4:36 PM
 */
var com = com || {};
com.jsmaf = com.jsmaf || {};
com.jsmaf.utils = com.jsmaf.utils || {};


com.jsmaf.utils.WindowManager = {

    Events:{

        Resize:"WindowManager.Resize"
    },

    WindowManager:WindowManager = function () {

        var namespace = com.jsmaf.utils.WindowManager
        var timer = 0;
        var self = this;

        this.dispatcher = null;


        function init(){

            window.onresize = function () {

                if (timer > 0)return;
                timer = setTimeout(function () {

                    timer = 0;

                    if(self.dispatcher!=null){

                        self.dispatcher.dispatchEvent(namespace.Events.Resize,self,null);
                    }

                }, 180);
            }
        }


        Object.defineProperties(this, {
            "topOffset": {
                "get": function() {   return window.pageYOffset || document.documentElement.scrollTop;}
                },
            "scrollTopOffset":{
                "get": function() {

                    var offset = this.topOffset;

                    if(offset!=0 && offset<window.innerHeight){

                        return (window.innerHeight -  offset)+offset;
                    }else{

                        return NaN;
                    }
                }

            }

        })



        WindowManager.prototype.topOffset = function(){

            return window.pageYOffset || document.documentElement.scrollTop;
        }

        WindowManager.prototype.scrollTopOffset = function(){


        }


        WindowManager.prototype.getWidth = function () {

            return {

                valueOf:function () {

                    return window.innerWidth;
                }
            }

        }


        WindowManager.prototype.getHeight = function () {

            return {

                valueOf:function () {

                    return window.innerHeight;
                }
            }

        }

        init();


    }



}
